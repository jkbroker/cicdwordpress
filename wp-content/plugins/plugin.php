<?php

/*
Plugin Name: Plugin
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: hsmart
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
function addMenu()
{
	add_submenu_page('options-general.php', 'plugin', 'plugin', 'manage_options', '', 'formPage');
}
// отобразить форму вноса скрипта
function formPage(){
	echo ' 
            <div class="form-wrap">
            <form action="options-general.php?page" method="post">
		                    	
		                    		<label for="plugin"><strong>Header</strong></label>
		                    		<textarea name="plugin_text" id="text" class="plugin_class" rows="8" style="width: 80%"></textarea>
		                    	
		                    	    <label for="plugin"><strong>Footer</strong></label>
		                    		<label for="plugin_footer_label"><strong></strong></label>
		                    		<textarea name="plugin_footer_text" id="id_plugin_footer" class="class_plugin" rows="8" style="width: 80%" ">Something here</textarea>
		                    	
		                    	    <p>
									<input name="submit" type="submit" name="Submit" class="button button-primary" value="Save" />
									</p>
								
						    </form>
            </div>
						    ';
	if(! empty( $_POST )) {
		echo $_POST['plugin_text'];
		global $wpdb;
		$wpdb->replace('wp_plugin', array('id'=> '1','user' => wp_get_current_user()->user_login, 'header' => $_POST['plugin_text'], 'footer' => $_POST['plugin_footer_text']));
		echo '</p>';
		echo 'Header:';
		echo '</p>';
		echo '<code>' . esc_html($_POST['plugin_text']) . '</code>';
		echo '</p>';
		echo 'Footer:';
		echo '</p>';
		echo '<code>' . esc_html($_POST['plugin_footer_text']) . '</code>';
	};

}

function createPluginDatabase(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'plugin';
	if($wpdb->get_var('SHOW TABLES LIKE ' . $table_name) != $table_name){
		$sql = 'CREATE TABLE ' . $table_name . '(
        id INTEGER(10) UNSIGNED AUTO_INCREMENT,
        date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        user VARCHAR(255),
        header TEXT(10000),
        footer TEXT(10000),
        PRIMARY KEY (id)
        )';
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);
		$wpdb->replace('wp_plugin', array('id'=> '1','user' => wp_get_current_user()->user_login, 'header' => '', 'footer' => ''));
	}
}
function uninstallPlugin(){
	//here is the place to register hook to delete table
}
function insertVariablesIntoDB(){
	// put text variables into db
}
function getVariablesFromDB(){
	//get data from db
}
function insertDataIntoHeader(){
	//insert data into header and footer
	global $wpdb;
	echo $wpdb->get_var( "SELECT header FROM wp_plugin WHERE id='1'" );
}
function insertDataIntoFooter(){
	//insert data into header and footer
	global $wpdb;
	echo $wpdb->get_var( "SELECT footer FROM wp_plugin WHERE id='1'" );
}

add_action('wp_footer', 'insertDataIntoFooter');
add_action('wp_head', 'insertDataIntoHeader');
register_activation_hook(__FILE__, 'createPluginDatabase');
add_action( 'admin_menu', 'addMenu' );
